### Tatra mountains with rayshader and rgl::rglwidget()

- [notebook preview](https://micstan.gitlab.io/share_zone/tatry_3d.nb.html)
- Playing with 3d terrain visualizations using [rayshader](https://github.com/tylermorganwall/rayshader) and [rgl](https://cran.r-project.org/web/packages/rgl/vignettes/WebGL.html) for an interactive WebGL plugin
- Elevation data with 100m resolution shared by [http://www.codgik.gov.pl](http://www.codgik.gov.pl)- 
